package com.shiyi.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyi.common.R;
import com.shiyi.domain.Friend;

/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe:
 */
public interface FriendService extends IService<Friend> {

    /**
     * 好友列表
     * @return
     */
    R selectFriendList(Integer userId);
}
