package com.shiyi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shiyi.common.R;
import com.shiyi.domain.User;
import com.shiyi.dto.UserLoginDTO;
import com.shiyi.mapper.UserMapper;
import com.shiyi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe:
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    private final UserMapper userMapper;

    @Override
    public R login(UserLoginDTO dto) {
        User user = userMapper.selectOne(new LambdaQueryWrapper<User>()
                .eq(User::getLoginName, dto.getUsername()).eq(User::getPassword, dto.getPassword()));
        if (ObjectUtils.isEmpty(user)){
            return R.fail("用户名或密码错误");
        }
        return R.success(user);
    }

    @Override
    public R search(String username) {
        List<User> list =  userMapper.selectUserByUsername(username);
        return R.success(list);
    }
}
