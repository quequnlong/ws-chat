package com.shiyi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyi.common.R;
import com.shiyi.domain.Music;

/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe:
 */
public interface MusicService extends IService<Music> {


    R selectMusicList();
}
