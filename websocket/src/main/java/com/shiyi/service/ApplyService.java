package com.shiyi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyi.common.R;
import com.shiyi.domain.Apply;

/**
 * @author: 拾壹
 * @date: 2023/12/15
 * @describe:
 */
public interface ApplyService extends IService<Apply> {
    R selectApplyList(Integer userId);

    R insertApply(Apply apply);

    R updateApply(Apply apply);
}
