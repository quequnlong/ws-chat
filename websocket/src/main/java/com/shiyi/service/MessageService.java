package com.shiyi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyi.common.R;
import com.shiyi.domain.Message;


/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe:
 */
public interface MessageService extends IService<Message> {

    /**
     * 获取消息列表
     * @param receiverId 接收者id
     * @param isGroup 是否群聊
     * @return
     */
    R selectMessageList(Integer userId,Integer receiverId, Integer isGroup);

    /**
     * 添加消息
     * @param message
     */
    void  insertMessage(Message message);
}
