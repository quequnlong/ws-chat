package com.shiyi.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author: 拾壹
 * @date: 2023/12/15
 * @describe:
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplyListVO {

    private Integer id;

    private String avatar;

    private String nickname;

    private Integer isGroup;

    private Integer status;

    private Date createTime;
}
