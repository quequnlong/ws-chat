package com.shiyi.vo;

import lombok.Data;

/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe:
 */
@Data
public class FriendListVO {

    private Integer id;

    private Integer userId;
    private Integer friendId;

    private String nickname;

    private String avatar;

    private Integer state;


}
