package com.shiyi.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe:
 */
@Data
public class MessageListVO {
    /**
     * 主键id
     */
    private Integer id;

    /**
     *  发送者id
     */
    private Integer senderId;


    /**
     *  发送者头像
     */
    private String senderAvatar;

    /**
     *  发送者昵称
     */
    private String senderNickname;

    /**
     *  接收者id
     */
    private Integer receiverId;

    /**
     *  发送者头像
     */
    private String receiverAvatar;

    /**
     *  消息内容
     */
    private String content;


    /***
     * 消息类型
     */
    private Integer type;

    /**
     * 消息状态
     */
    private Integer status;

    /**
     * 是否群聊  0：单聊 1群聊
     */
    private Integer isGroup;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
