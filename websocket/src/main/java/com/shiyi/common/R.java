package com.shiyi.common;


import lombok.Data;

/**
 * @author: 拾壹
 * @date: 2023/11/14
 * @describe:
 */
@Data
public class R {

    /**
     * 响应码：参考`ResultCode`
     */
    private Integer code;

    /**
     * 描述信息
     */
    private String message;

    /**
     * 响应中的数据
     */
    private Object data;

    public static R fail(Object data) {
        return new R(400,"操作失败", data);
    }

    public static R fail(Integer code, String message) {
        return new R(code,message, null);
    }

    public static R fail() {
        return new R(400,"操作失败", null);
    }

    public static R success(Object data) {
        return new R(200, "操作成功",data);
    }
    public static R success(Integer code, Object data) {
        return new R(code, "操作成功",data);
    }

    public static R success() {
        return new R(200, "操作成功",null);
    }

    public R(Integer code, String message, Object data) {
        this.code = code;
        this.data = data;
        this.message = message;
    }
}
