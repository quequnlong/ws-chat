package com.shiyi.controller;

import com.shiyi.common.R;
import com.shiyi.service.FriendService;
import com.shiyi.service.MusicService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author: 拾壹
 * @date: 2023/11/14
 * @describe: 音乐控制器
 */
@RestController
@RequestMapping("api/music")
@RequiredArgsConstructor
public class MusicController {

    private final MusicService musicService;


    @GetMapping("index")
    public R index() {
        return musicService.selectMusicList();
    }

}
