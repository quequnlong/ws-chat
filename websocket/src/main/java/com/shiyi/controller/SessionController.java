package com.shiyi.controller;

import com.shiyi.common.R;
import com.shiyi.service.FriendService;
import com.shiyi.service.SessionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author: 拾壹
 * @date: 2023/11/14
 * @describe: 好友控制器
 */
@RestController
@RequestMapping("api/session")
@RequiredArgsConstructor
public class SessionController {

    private final SessionService sessionService;


    @GetMapping("index")
    public R index(Integer userId) {
        return sessionService.selectSessionList(userId);
    }

}
