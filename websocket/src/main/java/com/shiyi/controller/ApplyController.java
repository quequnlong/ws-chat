package com.shiyi.controller;

import com.shiyi.common.R;
import com.shiyi.domain.Apply;
import com.shiyi.service.ApplyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * @author: 拾壹
 * @date: 2023/11/14
 * @describe:
 */
@RestController
@RequestMapping("api/apply")
@RequiredArgsConstructor
public class ApplyController {

    private final ApplyService applyService;


    @GetMapping("index")
    public R index(Integer userId) {
        return applyService.selectApplyList(userId);
    }

    @PostMapping("/")
    public R apply(@RequestBody Apply apply) {
        return applyService.insertApply(apply);
    }

    @PutMapping("/")
    public R updateApply(@RequestBody Apply apply) {
        return applyService.updateApply(apply);
    }

}
