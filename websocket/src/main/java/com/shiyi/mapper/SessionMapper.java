package com.shiyi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shiyi.domain.Session;
import com.shiyi.vo.SessionListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe:
 */
public interface SessionMapper extends BaseMapper<Session> {


    Page<SessionListVO> selectSessionList(@Param("page") IPage<Object> page,@Param("userId") Integer userId);
}
