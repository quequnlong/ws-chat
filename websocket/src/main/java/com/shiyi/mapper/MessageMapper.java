package com.shiyi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shiyi.domain.Message;
import com.shiyi.vo.MessageListVO;
import org.apache.ibatis.annotations.Param;


/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe:
 */
public interface MessageMapper extends BaseMapper<Message> {

    Page<MessageListVO> selectMessageList(@Param("page") IPage<Object> page, @Param("userId") int userId, @Param("receiverId") int receiverId, @Param("isGroup") int isGroup);

}
