package com.shiyi.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe: 用户实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    /**
     * 主键id
     */
    private Integer id;

    /**
     *  用户昵称
     */
    private String nickname;

    /**
     *  用户头像
     */
    private String avatar;

    /**
     *  登录用户名
     */
    private String loginName;

    /**
     *  登录用户名
     */
    private String password;

    /**
     *  在线状态
     */
    private Integer state;


    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
