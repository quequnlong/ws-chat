package com.shiyi.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe: 音乐实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Music {

    /**
     * 主键id
     */
    private Integer id;

    /**
     *  音乐名
     */
    private String name;

    /**
     *  作者
     */
    private String author;

    /**
     *  mp3地址
     */
    private String url;

    /**
     *  封面图地址
     */
    private String coverImg;


}
