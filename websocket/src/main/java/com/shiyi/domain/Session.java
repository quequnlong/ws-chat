package com.shiyi.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe: 会话实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Session {

    /**
     * 主键id
     */
    private Integer id;

    /**
     *  用户id
     */
    private Integer userId;

    /**
     *  好友id
     */
    private Integer sessionId;

    /**
     *  会话类型 1 单聊 2群聊
     */
    private Integer type;

    /**
     *  最后消息
     */
    private String lastMessage;


    /**
     * 最后消息发送时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastTime;

}
