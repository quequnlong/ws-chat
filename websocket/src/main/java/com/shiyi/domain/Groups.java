package com.shiyi.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: 拾壹
 * @date: 2023/12/14
 * @describe: 群
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Groups {

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 昵称
     */
    private String name;


    /**
     * 头像
     */
    private String avatar;

    /**
     * 群人数
     */
    private Integer memberNum;
}
