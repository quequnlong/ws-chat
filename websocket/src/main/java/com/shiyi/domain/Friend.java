package com.shiyi.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author: 拾壹
 * @date: 2023/12/13
 * @describe: 好友实体类
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Friend {

    /**
     * 主键id
     */
    private Integer id;

    /**
     *  用户id
     */
    private Integer userId;

    /**
     *  好友id
     */
    private Integer friendId;

    /**
     *  关系状态 1：正常 0：拉黑
     */
    private Integer state;


    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

}
