package com.shiyi.exception;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.shiyi.common.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.shiyi.common.ResultCode.ERROR;


/**
 * @author blue
 * @date 2022/3/11
 * @apiNote
 */
@ControllerAdvice(basePackages = "com.shiyi")
public class GlobalException {

    private static final Logger logger = LoggerFactory.getLogger(GlobalException.class);

    // 业务异常
    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public R BusinessExceptionHandler(BusinessException ex) {
        if (ex.getCode() != -1) {
            logger.error("code : " + ex.getCode() + " msg : " + ex.getMessage(), ex);
        }
        if(StringUtils.isBlank(ex.getLocalizedMessage())||StringUtils.isBlank(ex.getMessage())){
            return R.fail(ERROR.getCode(), ERROR.getDesc());
        }
        return R.fail(ex.getCode(), ex.getMessage());
    }

    // Assert业务异常
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    public R AssertExceptionHandler(IllegalArgumentException ex) {
        logger.error( " msg : " + ex.getMessage(), ex);
        if(StringUtils.isBlank(ex.getLocalizedMessage())){
            return R.fail(ERROR.getCode(),ERROR.getDesc());
        }
        return R.fail(ex.getMessage());
    }



    // java异常异常
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R ExceptionHandler(Exception ex) {
        logger.error( " msg : " + ex.getMessage(), ex);
        if(StringUtils.isBlank(ex.getLocalizedMessage())){
            return R.fail();
        }
        return R.fail();
    }
}
