import request from '@/utils/request'

export function getApplyList(params) {
    return request({
        url: '/api/apply/index',
        method: 'get',
        params: params
    })
}
export function postApply(data) {
    return request({
        url: '/api/apply/',
        method: 'post',
        data
    })
}
export function updateApply(data) {
    return request({
        url: '/api/apply/',
        method: 'put',
        data
    })
}

