import request from '@/utils/request'

export function getFriendList(params) {
    return request({
        url: '/api/friend/index',
        method: 'get',
        params: params
    })
}

