import request from '@/utils/request'

export function getSessionList(params) {
    return request({
        url: '/api/session/index',
        method: 'get',
        params: params
    })
}

