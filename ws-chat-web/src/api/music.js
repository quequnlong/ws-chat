import request from '@/utils/request'

export function getMusicList(params) {
    return request({
        url: '/api/music/index',
        method: 'get',
        params: params
    })
}

