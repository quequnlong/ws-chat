import Vue from "vue";
import VueRouter from "vue-router";
import Layout from '@/components/layout/index.vue'

Vue.use(VueRouter);
const routes = [
    {
        path: "/",
        component: Layout,
        meta: {
            title: ""
        },
        children: [
            {
                path: "/",
                component: resolve => require(["@/views/home/index.vue"], resolve),
                meta: {
                    title: ""
                }
            },
            {
                path: "home",
                component: resolve => require(["@/views/home/home.vue"], resolve),
                meta: {
                    title: ""
                }
            },
        ],
    }
];

const router = new VueRouter({
    mode: "history",
    routes
});
// // 获取原型对象push函数
// const originalPush = VueRouter.prototype.push

// // 获取原型对象replace函数
// const originalReplace = VueRouter.prototype.replace

// // 修改原型对象中的push函数
// VueRouter.prototype.push = function push(location) {
//     return originalPush.call(this, location).catch(err => err)
// }

// // 修改原型对象中的replace函数
// VueRouter.prototype.replace = function replace(location) {
//     return originalReplace.call(this, location).catch(err => err)
// }

router.beforeEach((to, from, next) => {
    if (to.meta.title) {
        document.title = to.meta.title
    }
    next()
})
export default router;